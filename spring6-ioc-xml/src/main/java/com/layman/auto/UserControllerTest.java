package com.layman.auto;

import com.layman.auto.controller.UserController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserControllerTest {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean-auto.xml");
        UserController userController = applicationContext.getBean("userController", UserController.class);
        userController.addUser();
    }
}
