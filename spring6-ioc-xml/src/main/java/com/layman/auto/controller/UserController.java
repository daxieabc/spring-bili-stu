package com.layman.auto.controller;

import com.layman.auto.service.UserService;

public class UserController {

    private UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void addUser() {
        System.out.println("controller add user");
        userService.addUserService();
    }
}
