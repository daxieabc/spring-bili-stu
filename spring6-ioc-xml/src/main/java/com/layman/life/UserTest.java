package com.layman.life;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserTest {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean-life.xml");
        User user = applicationContext.getBean("user", User.class);
        System.out.println(user.toString());
        applicationContext.close();
    }
}
