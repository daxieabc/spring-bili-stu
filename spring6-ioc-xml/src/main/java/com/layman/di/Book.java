package com.layman.di;

public class Book {

    private String name;

    private String author;

    private String others;

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        System.out.println("set name 调用");
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        System.out.println("set author 调用");
        this.author = author;
    }

    public Book() {
    }

    public Book(String name, String author) {
        System.out.println("book  构造参数调用 ...");
        this.name = name;
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", others='" + others + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Book book = new Book("小叔", "张三");

    }
}
