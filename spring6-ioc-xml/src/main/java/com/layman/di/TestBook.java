package com.layman.di;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestBook {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:di.xml");
        Book book = (Book) applicationContext.getBean("book");
        System.out.println(book);

        Book book1 = (Book) applicationContext.getBean("bookCun");
        System.out.println(book1);
    }
}
