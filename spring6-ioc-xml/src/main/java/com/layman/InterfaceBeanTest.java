package com.layman;

import com.layman.bean.UserDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class InterfaceBeanTest {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:beans.xml");
        UserDao userDao = applicationContext.getBean(UserDao.class);
        System.out.println("接口类型获取bean=" + userDao);
        userDao.run();
    }
}
