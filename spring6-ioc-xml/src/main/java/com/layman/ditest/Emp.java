package com.layman.ditest;

import java.util.List;

public class Emp {

    private String ename;

    private Integer age;

    // 员工属于某个部门
    private Dept dept;

    private List<String> loves;

    public List<String> getLoves() {
        return loves;
    }

    public void setLoves(List<String> loves) {
        this.loves = loves;
    }

    public void work () {
        dept.info();
        System.out.println(String.format("emp work: name=%s, age=%d", ename, age));
        System.out.println(loves);
    }


    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "ename='" + ename + '\'' +
                ", age=" + age +
                ", dept=" + dept +
                ", loves=" + loves +
                '}';
    }
}
