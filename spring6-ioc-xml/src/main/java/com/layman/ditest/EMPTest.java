package com.layman.ditest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class EMPTest {

    public static void main(String[] args) {
//        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:bean-di-test.xml");
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:bean-di-arry.xml");
        Emp bean = applicationContext.getBean("emp", Emp.class);
        bean.work();
//        Emp empInner = applicationContext.getBean("emp-inner", Emp.class);
//        empInner.work();
//        Emp empInsert = applicationContext.getBean("emp-insert", Emp.class);
//        empInsert.work();
    }
}
