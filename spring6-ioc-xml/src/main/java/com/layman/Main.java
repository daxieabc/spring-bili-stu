package com.layman;

import com.layman.bean.User;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:beans.xml");
        User user = (User) applicationContext.getBean("user");
        System.out.println("根据id获取bean = " + user);

        User bean = applicationContext.getBean(User.class);
        System.out.println("根据类型获取bean = " + bean);

        User user1 = applicationContext.getBean("user", User.class);
        System.out.println("根据id和类型获取bean = " + user1);
    }


}