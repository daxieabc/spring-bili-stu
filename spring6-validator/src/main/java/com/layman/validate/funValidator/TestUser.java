package com.layman.validate.funValidator;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestUser {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ValidationConfig.class);
        MyService myService = applicationContext.getBean("myService", MyService.class);
        User user = new User();
        user.setName("lucy");
        user.setPhone("15377186929");
        user.setMessage("123 123");
        myService.testMethod(user);
    }
}
