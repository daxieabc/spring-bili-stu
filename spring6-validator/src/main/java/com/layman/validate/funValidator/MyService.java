package com.layman.validate.funValidator;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
public class MyService {

    public String testMethod(@NotNull @Valid User user) {
        System.out.println(user.toString());
        return user.toString();
    }
}
