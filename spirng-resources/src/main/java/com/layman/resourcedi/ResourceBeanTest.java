package com.layman.resourcedi;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ResourceBeanTest {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("resourcedi.xml");
        ResourceBean resourceBean = applicationContext.getBean("resourceBean", ResourceBean.class);
        resourceBean.parse();
    }
}
