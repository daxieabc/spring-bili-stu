package com.layman.resources;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.UrlResource;

import java.io.IOException;
import java.io.InputStream;

public class ClasspathResourcesDemo {

    public static void loadClasspathResources(String path) throws IOException {
        // 创建一个 Resource 对象
        ClassPathResource resource = new ClassPathResource(path);
        // 获取文件名
        System.out.println("resource.getFileName = " + resource.getFilename());
        // 获取文件描述
        System.out.println("resource.getDescription = "+ resource.getDescription());
        //获取文件内容
        InputStream in = resource.getInputStream();
        byte[] b = new byte[1024];
        while(in.read(b)!=-1) {
            System.out.println(new String(b));
        }
    }

    public static void main(String[] args) throws IOException {
//        loadUrlResources("https://www.baidu.com/img/flexible/logo/pc/result.png");
//        loadUrlResources("http://www.baidu.com");
        loadClasspathResources("info.txt");
    }

}
