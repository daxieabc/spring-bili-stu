package com.layman.resources;

import org.springframework.core.io.UrlResource;

/**
 * 演示UrlResources 访问网络资源
 */
public class UrlResourcesDemo {

    public static void loadUrlResources(String path) {
        // 创建一个 Resource 对象
        UrlResource url = null;
        try {
            url = new UrlResource(path);
            // 获取资源名
            System.out.println(url.getFilename());
            System.out.println(url.getURI());
            // 获取资源描述
            System.out.println(url.getDescription());
            //获取资源内容
            System.out.println(url.getInputStream().read());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
//        loadUrlResources("https://www.baidu.com/img/flexible/logo/pc/result.png");
//        loadUrlResources("http://www.baidu.com");
        loadUrlResources("file:info.txt");
    }
}
