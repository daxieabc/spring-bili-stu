package com.layman.tx;

import com.layman.tx.config.SpringConfig;
import com.layman.tx.controller.BookController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestAnnoSpring {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        BookController bookController = applicationContext.getBean("bookController", BookController.class);
        Integer[] booksIds = {1, 2};
        bookController.buyBooks(1, booksIds);
    }
}
