package com.layman.tx.dao;

public interface BookDao {
    Integer getPriceByBookId(int bookId);

    void updateStock(int bookId);

    void updateUserBalance(int userId, Integer price);
}
