package com.layman.tx.controller;

import com.layman.tx.service.BookService;
import com.layman.tx.service.CheckoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class BookController {

    @Autowired
    private BookService bookService;


    @Autowired
    private CheckoutService checkoutService;

    public void buyBook(int userId, int bookId) {
        bookService.buyBook(bookId, userId);
    }

    public void buyBooks(int userId, Integer[] bookIds) {
        checkoutService.checkout(bookIds, userId);
    }
}
