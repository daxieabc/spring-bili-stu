package com.layman.tx.service;

public interface BookService {

    void buyBook(int bookId, int userId);
}
