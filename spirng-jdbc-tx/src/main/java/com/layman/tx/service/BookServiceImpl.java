package com.layman.tx.service;

import com.layman.tx.dao.BookDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookDao bookDao;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void buyBook(int bookId, int userId) {
        // 查图画书价格
        Integer price = bookDao.getPriceByBookId(bookId);
        // 图书库存 -1
        bookDao.updateStock(bookId);
        // 用户余额 - 图书价格
        bookDao.updateUserBalance(userId, price);
    }
}
