package com.layman.tx;

import com.layman.tx.config.SpringConfig;
import com.layman.tx.controller.BookController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

//@SpringJUnitConfig(locations = "classpath:spring.xml")
@SpringJUnitConfig(SpringConfig.class)
public class TransactionTest {

    @Autowired
    private BookController bookController;

    @Test
    public void buyBookTest() {
        bookController.buyBook(1, 1);
    }

    @Test
    public void buyBooksTest() {
        Integer[] booksIds = {1, 2};
        bookController.buyBooks(1, booksIds);
    }


}
