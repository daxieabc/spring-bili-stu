package com.layman;

import com.layman.bean.HelloWorld;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.lang.reflect.InvocationTargetException;


public class HelloWorldTest {

    Logger logger = LoggerFactory.getLogger(HelloWorldTest.class);

    @Test
    public void testHelloWorld() {
        ApplicationContext applicationContext = new FileSystemXmlApplicationContext("classpath:beans.xml");
        HelloWorld helloWorld = (HelloWorld) applicationContext.getBean("helloWorld");
        logger.info("test hello world success");
        helloWorld.sayHello();
    }

    // 反射创建对象

    @Test
    public void testReflect() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class aClass = Class.forName("com.layman.bean.HelloWorld");
        // 先获取构造函数 再创建对象
        HelloWorld helloWorld = (HelloWorld) aClass.getDeclaredConstructor().newInstance();
        helloWorld.sayHello();
    }
}
