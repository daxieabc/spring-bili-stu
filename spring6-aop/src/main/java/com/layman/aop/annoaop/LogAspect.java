package com.layman.aop.annoaop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

// 切面类
@Aspect
@Component
public class LogAspect {

    // 设置切入点和通知类型
    // 切入点表达式:
    // execution (访问修饰符 增强方法返回对象        增强方法所在类全路径   类名     方法名   参数列表)
    // execution (public    int                 com.layman.*..      *    .   *        (..))
    // 通知类型:
    //      前置通知 @Before(value = "切入点表达式")
    //      返回通知 @AfterReturning()
    //      异常通知 @AfterThrowing()
    //      后置通知@After()
    //      环绕通知@Around()

    @Before(value = "execution (public int com.layman.aop.annoaop.CalculatorImpl.* (..))")
    public void  beforeMethod(JoinPoint joinPoint) {
        String name = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        System.out.println(String.format("Logger ===> 前置通知, method name = %s, args=%s", name, Arrays.toString(args)));
    }

    @After(value = "execution (* com.layman.aop.annoaop.CalculatorImpl.* (..))")
    public void afterMethod(JoinPoint joinPoint) {
        String name = joinPoint.getSignature().getName();
        System.out.println(String.format("Logger ===> 后置通知, method name = %s", name));
    }

    @AfterReturning(value = "execution (* com.layman.aop.annoaop.CalculatorImpl.* (..))", returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result) {
        String name = joinPoint.getSignature().getName();
        System.out.println(String.format("Logger ===> 返回后通知, method name = %s, 返回 = %s", name, result));
    }

    @AfterThrowing(value = "execution (* com.layman.aop.annoaop.CalculatorImpl.* (..))", throwing = "ex")
    public void afterException(JoinPoint joinPoint, Throwable ex) {
        String name = joinPoint.getSignature().getName();
        System.out.println(String.format("Logger ===> 异常后通知, method name = %s, 异常信息:%s", name, ex));
    }

    @Around(value = "pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) {
        String name = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        String argString = Arrays.toString(args);
        Object result = null;
        try {
            System.out.println(String.format("环绕通知, 在目标方法%s之前执行", name));
            result = joinPoint.proceed();
            System.out.println(String.format("环绕通知, 在目标方法%s返回值之后执行", name));
        } catch (Throwable ex) {
            System.out.println(String.format("环绕通知, 在目标方法%s 出现异常后执行, 异常=%s", name, ex));
        } finally {
            System.out.println(String.format("环绕通知, 在目标方法%s 执行完毕之后后执行", name));
        }
        return result;
    }

    @Pointcut(value = "execution (* com.layman.aop.annoaop.CalculatorImpl.* (..))")
    public void pointcut() {

    }
}
