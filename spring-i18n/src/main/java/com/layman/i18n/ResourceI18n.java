package com.layman.i18n;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceI18n {

//    public static void main(String[] args) {
//        ResourceBundle resourceBundle = ResourceBundle.getBundle("message", new Locale("en", "GB"));
//        String test = resourceBundle.getString("test");
//        System.out.println(test);
//    }

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        Object[] objects = new Object[] {"layman", new Date().toString()};
        String wel = applicationContext.getMessage("wel", objects, Locale.UK);
        System.out.println(wel);
    }
}
