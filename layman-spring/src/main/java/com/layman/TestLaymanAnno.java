package com.layman;

import com.layman.bean.LaymanApplicationContext;
import com.layman.bean.impl.LaymanAnnotationApplicationContext;
import com.layman.service.UserService;

public class TestLaymanAnno {
    public static void main(String[] args) {
        LaymanApplicationContext context = new LaymanAnnotationApplicationContext("com.layman");
        UserService userService = context.getBean(UserService.class);
        System.out.println(userService);
        userService.add();
    }
}
