package com.layman.service.impl;

import com.layman.anno.LaymanBean;
import com.layman.anno.LaymanDi;
import com.layman.dao.UserDao;
import com.layman.service.UserService;

@LaymanBean
public class UserServiceImpl implements UserService {

    @LaymanDi
    private UserDao userDao;

    @Override
    public void add() {
        userDao.add();
        System.out.println("userServiceImpl add ...");
    }
}
