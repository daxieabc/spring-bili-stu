package com.layman.bean.impl;

import com.layman.anno.LaymanBean;
import com.layman.anno.LaymanDi;
import com.layman.bean.LaymanApplicationContext;
import com.layman.service.UserService;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class LaymanAnnotationApplicationContext implements LaymanApplicationContext {

    // 创建一个map集合 放bean对象
    private Map<Class<?>, Object> beanFactory = new HashMap<>();

    private String rootPath;

    // 返回创建好的对象
    @Override
    public <T> T getBean(Class<T> clazz) {
        return (T) beanFactory.get(clazz);
    }

    // 设置包的扫描的规则
    // 当前包 及其子包， 那个类 有 @LaymanBean 注解 则通过反射实例化
    public LaymanAnnotationApplicationContext(String basePackage) {
        // 1. 把路劲中的.替换成\
        String packagePath = basePackage.replaceAll("\\.", "\\\\");
        // 2.获取包的绝对路径
        try {
            Enumeration<URL> urls = Thread.currentThread().getContextClassLoader().getResources(packagePath);
            while (urls.hasMoreElements()) {
                URL url = urls.nextElement();
                String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                // 获取包前面的部分
                rootPath = filePath.substring(0, filePath.indexOf(packagePath));
                // 包扫描过程
                loadBean(new File(filePath));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        loadDi();
    }

    public static void main(String[] args) {
        LaymanApplicationContext applicationContext =
                new LaymanAnnotationApplicationContext("com.layman");
        Object bean = applicationContext.getBean(UserService.class);
    }

    private void loadBean(File classFile) throws Exception {
        // 1. 判断当前的内容是否为文件夹  是继续向下扫描 不是则不进行向下扫描
        if (classFile.isDirectory()) {
            // 2. 获取文件夹里面的内容
            File[] childrenFiles = classFile.listFiles();

            // 3. 判断文件夹里面是否为空 为空则返回
            if (childrenFiles == null || childrenFiles.length == 0) {
                return;
            }
            // 4. 如果文件夹里面不为空. 遍历文件夹里面的所有内容
            for (File childFile : childrenFiles) {
                // 4.1 遍历得到每个File 对象,继续判断, 如果还是文件夹, 递归遍历
                if (childFile.isDirectory()) {
                    // 递归
                    loadBean(childFile);
                } else {
                    // 4.2 遍历得到的FIle 对象不是文件夹, 是文件
                    // 4.3 得到包路劲+类名称部分 - 字符串截取
                    String pathWithClass = childFile.getAbsolutePath().substring(rootPath.length() - 1);
                    // 4.4 当前文件的类型是否是 .class
                    if (pathWithClass.endsWith(".class")) {
                        // 4.5 如果是.class类型, 把路径\替换成. 把.class 去掉
                        //         com.layman.service.UserServiceImpl
                        String allName = pathWithClass.replaceAll("\\\\", ".").replace(".class", "");
                        // 4.6 判断类上面是否有注解 @LaymanBean 如果有 使用反射进行实例化
                        // 4.6.1 获取类的class
                         Class<?> clazz = Class.forName(allName);
                        // 4.6.2 判断是不是接口
                        if (!clazz.isInterface()) {
                            // 4.6.3 判断类上面是否有注解 @LaymanBean
                            LaymanBean laymanBean = clazz.getAnnotation(LaymanBean.class);
                            if (laymanBean != null) {
                                // 4.6.4 实例化
                                Object instance = clazz.getConstructor().newInstance();
                                // 4.7 把对象实例化之后 放到 beanFactory 的 Map 中
                                // 4.7.1 判断这个类如果有接口, 让接口的class作为map的key
                                if (clazz.getInterfaces().length > 0) {
                                    Class<?>[] interfaces = clazz.getInterfaces();
                                    for (Class<?> interfaceClazz : interfaces) {
                                        beanFactory.put(interfaceClazz, instance);
                                    }
                                } else {
                                    beanFactory.put(clazz, instance);
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    private void loadDi() {
        // 实例化的对象都是在beanFactory的map里面
        // 1. 遍历beanFactory 集合
        Set<Map.Entry<Class<?>, Object>> entries = beanFactory.entrySet();
        for (Map.Entry<Class<?>, Object> entry : entries) {
            // 2. 获取map集合里面的每个对象(value), 每个对象的属性获取到
            Object value = entry.getValue();
            // 获取对象的class
            Class<?> clazz = value.getClass();
            // 获取对象的属性
            Field[] declaredFields = clazz.getDeclaredFields();
            // 3. 遍历得到每一个对象属性的数组, 得到每个属性
            for (Field field : declaredFields) {
                // 4. 判断属性上面是否有@LaymanDi注解
                LaymanDi annotation = field.getAnnotation(LaymanDi.class);
                if (annotation != null) {
                    // 私有属性 设置可以修改值
                    field.setAccessible(true);
                    try {
                        // 5. 如果有@LaymanDi注解 把对象进行注入
                        Object object = beanFactory.get(field.getType());
                        field.set(value, object);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }

    }
}
