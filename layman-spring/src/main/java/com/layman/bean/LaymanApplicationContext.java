package com.layman.bean;

public interface LaymanApplicationContext {

    <T> T  getBean(Class<T> clazz);
}
